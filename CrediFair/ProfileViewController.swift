//
//  ProfileViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 12/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    var rest : RestClient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rest = RestClient()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
