//
//  Application4ViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 14/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class Application4ViewController: UIViewController {
    
    @IBOutlet weak var bankDetailSwitch: UISegmentedControl!
    @IBOutlet weak var ibanInput: UITextField!
    @IBOutlet weak var sortCodeInput: UITextField!
    @IBOutlet weak var accountNumberInput: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    
    var rest: RestClient?
    var router: RouterService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showIban()
        
        rest = RestClient()
        router = RouterService()
        
        let styler = StylingService()
        styler.styleButton(continueButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showIban() {
        dispatch_async(dispatch_get_main_queue()) {
            self.ibanInput.hidden = false
            self.sortCodeInput.hidden = true
            self.accountNumberInput.hidden = true
        }
    }
    
    func showSort() {
        dispatch_async(dispatch_get_main_queue()) {
            self.ibanInput.hidden = true
            self.sortCodeInput.hidden = false
            self.accountNumberInput.hidden = false
        }
    }
    
    @IBAction func bankDetailsSwitchPushed(sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 0) {
            // Iban
            showIban()
        } else {
            // Sort
            showSort()
        }
    }
    
    @IBAction func continuePushed(sender: UIButton) {
        var request = [String:String]()
        if (bankDetailSwitch.selectedSegmentIndex == 0) {
            request["iban"] = ibanInput.text
        } else {
            request["sort"] = sortCodeInput.text
            request["accountNumber"] = accountNumberInput.text
        }
        rest?.postBankDetails(request, onComplete: { data in
            let step = self.router?.mapApiStepResponseToRoute(data)
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier(step!, sender: self)
            }
        })
    }
    
}