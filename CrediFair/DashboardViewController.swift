//
//  DashboardViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 12/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class DashboardViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Modify the tab buttons
        let tabBarItems = self.tabBar.items
        for (index, tabBarItem) in tabBarItems!.enumerate() {
            if (index == 0) {
                tabBarItem.title = "Balance"
                tabBarItem.image = UIImage(named: "balance-icon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
            } else if(index == 1) {
                tabBarItem.title = "Profile"
                tabBarItem.image = UIImage(named: "profile-icon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
