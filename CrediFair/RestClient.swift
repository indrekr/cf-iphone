//
//  RestClient.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 20/01/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit
import SwiftHTTP

class RestClient {
    
    var oauth : OauthManager?
    
    init() {
        oauth = OauthManager()
    }
    
    private func parseJSON(data: NSData) -> AnyObject {
        do {
            let parsed = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
            return parsed
        } catch {
            print("Failed parsing json")
        }
        return false
    }
    
    func registerUser(country: String, email: String, password: String, onComplete: (data: AnyObject) -> ()) {
        self.getClientToken((oauth?.getClientAuthorization())!, onComplete: { token in
            let token = token["access_token"] as! String
            let headers = [
                "Authorization": "Bearer " + token
            ]
            let params = ["email" : email, "password" : password, "country": country]
            do {
                let opt = try HTTP.POST(ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue, parameters: params, headers: headers, requestSerializer: JSONParameterSerializer())
                opt.start { response in
                    //callback
                    let json = self.parseJSON(response.data)
                    
                    if let val = json["error"] {
                        if let x = val {
                            print(x)
                        } else {
                            onComplete(data: json)
                        }
                    } else {
                        onComplete(data: json)
                    }
                }
            } catch let error {
                print("got an error creating the request: \(error)")
            }
            
        })
    }
    
    func getClientToken(accessToken: String, onComplete: (data: AnyObject) -> ()) {
        let headers = [
            "Authorization": "Basic " + accessToken
        ]
        do {
            let opt = try HTTP.POST(ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.OAUTH_ENDPOINT.rawValue + "?grant_type=client_credentials", headers: headers)
                opt.start { response in
                    //callback
                    onComplete(data: self.parseJSON(response.data))
                }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    
    func getUserData(onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.GET(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.ACCOUNT_INFO.rawValue, headers: headers)
                opt.start { response in
                    //callback
                    onComplete(data: self.parseJSON(response.data))
                }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    func getStep(accessToken: String, onComplete: (data: AnyObject) -> ()) {
        let headers = [
            "Authorization": "Bearer " + accessToken
        ]
        do {
            let opt = try HTTP.GET(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.GET_STEP.rawValue, headers: headers)
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    func getStepData(onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.GET(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.STEP_DATA.rawValue, headers: headers)
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    
    func postStartApplication(accessToken: String, onComplete: (data: AnyObject) -> ()) {
        let headers = [
            "Authorization": "Bearer " + accessToken
        ]
        do {
            let opt = try HTTP.POST(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.START_APPLICATION.rawValue, headers: headers)
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    func putStepData(params: [String:String], onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.PUT(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.SUBMIT_DATA.rawValue, headers: headers, parameters: params, requestSerializer: JSONParameterSerializer())
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    func uploadFile(data: NSData, mimeType: String, onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.POST(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.UPLOAD_FILE.rawValue, headers: headers, parameters: ["file": Upload(data: data, fileName : "file", mimeType: mimeType)])
            opt.start { response in
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
        
    }
    
    func postIdDocsDone(onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.POST(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.ID_DOCS_DONE.rawValue, headers: headers)
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    func getFormCriteria(onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.GET(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.FORM_CRITERIA.rawValue, headers: headers)
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }
    
    func requestLoan(amount: CGFloat, onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.POST(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.REQUEST_LOAN.rawValue + "/" + String(amount), headers: headers)
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
        
    }
    
    func postBankDetails(bankDetails: [String:String], onComplete: (data: AnyObject) -> ()) {
        let accessToken = oauth?.tokenStore.retrieveAccessToken()
        let headers = [
            "Authorization": "Bearer " + (accessToken?.accessToken)!
        ]
        do {
            let opt = try HTTP.POST(
                ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.ACCOUNT_ENDPOINT.rawValue + ApiEndpointEnum.POST_BANK_DETAILS.rawValue, headers: headers, parameters: bankDetails, requestSerializer: JSONParameterSerializer())
            opt.start { response in
                //callback
                onComplete(data: self.parseJSON(response.data))
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
        
    }
    
    
}
