//
//  Application1ViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 11/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class Application1ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var educationPicker: UIPickerView!
    @IBOutlet weak var maritalStatusPicker: UIPickerView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var firstNameInput: UITextField!
    @IBOutlet weak var lastNameInput: UITextField!
    @IBOutlet weak var dobInput: UIDatePicker!
    @IBOutlet weak var phoneNumberInput: UITextField!
    
    var educationMapData: [String: String] = [String:String]()
    var maritalStatusMapData: [String: String] = [String:String]()
    
    var educationData: [String] = [String]()
    var maritalStatusData: [String] = [String]()
    
    var oauth : OauthManager?
    var rest : RestClient?
    var router : RouterService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let styler = StylingService()
        oauth = OauthManager()
        rest = RestClient()
        router = RouterService()
        
        // Style stuff
        styler.styleButton(continueButton)
        
        // Input data into the Array:
        educationMapData = ["Primary": "PRIMARY", "Basic": "BASIC", "Vocational": "VOCATIONAL", "Secondary": "SECONDARY", "Higher": "HIGHER"]
        maritalStatusMapData = ["Single": "SINGLE", "Married": "MARRIED", "Cohabitant": "COHABITANT", "Divorced": "DIVORCED", "Widow": "WIDOW"]
        educationData = educationMapData.keys.reverse()
        maritalStatusData = maritalStatusMapData.keys.reverse()
        
        // Connect data:
        self.educationPicker.delegate = self
        self.educationPicker.dataSource = self
        self.maritalStatusPicker.delegate = self
        self.maritalStatusPicker.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 0 {
            return educationData.count
        } else {
            return maritalStatusData.count
        }
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return educationData[row]
        } else {
            return maritalStatusData[row]
        }
    }
    
    @IBAction func continueButtonPushed(sender: UIButton) {
        let selectedEducation = educationMapData[educationData[educationPicker.selectedRowInComponent(0)]]
        let selectedMaritalStatus = maritalStatusMapData[maritalStatusData[maritalStatusPicker.selectedRowInComponent(0)]]
        
        var request : [String:String] = [String:String]()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateString = dateFormatter.stringFromDate(dobInput.date)
        
        request["FIRST_NAME"] = firstNameInput.text
        request["LAST_NAME"] = lastNameInput.text
        request["DATE_OF_BIRTH"] = dateString
        request["PHONE_NUMBER"] = phoneNumberInput.text
        request["EDUCATION"] = selectedEducation
        request["MARITAL_STATUS"] = selectedMaritalStatus
        
        rest?.putStepData(request, onComplete: { data in
            let step = self.router?.mapApiStepResponseToRoute(data)
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier(step!, sender: self)
            }
        })
    }

}