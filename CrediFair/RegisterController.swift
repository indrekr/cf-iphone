//
//  RegisterController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 21/01/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class RegisterController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: Properties
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var countryPicker: UIPickerView!
    
    var countryMapData: [String: String] = [String:String]()
    var countryData: [String] = [String]()
    
    var oauth: OauthManager?
    var rest: RestClient?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let styler = StylingService()
        // Customize buttons
        styler.styleButton(registerButton)
        
        // Setup the app with server
        oauth = OauthManager()
        rest = RestClient()
        
        // Input data into the Array:
        countryMapData = ["United Kingdom": "GB", "Estonia": "EE"]
        countryData = countryMapData.keys.reverse()
        
        // Connect data:
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryData[row]
    }
    
    // MARK: Actions
    @IBAction func registerButtonPushed(sender: UIButton) {
        let selectedCountry = countryMapData[countryData[countryPicker.selectedRowInComponent(0)]]
        self.rest?.registerUser(selectedCountry!, email: self.emailTextField.text!,
            password: self.passwordTextField.text!, onComplete: { data in
                // Get the access token, for heimdallr to store it
                self.oauth?.getUserAccessToken(self.emailTextField.text!, password: self.passwordTextField.text!, onComplete: { token in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.performSegueWithIdentifier(SegueStepEnum.GET_STARTED.rawValue, sender: self)
                    }
                    
                })
        })
    }
}

