//
//  DashboardController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 21/01/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class GetStartedController: UIViewController {
    
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var getStartedButton: UIButton!
    
    var oauth : OauthManager?
    var rest : RestClient?
    var router : RouterService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oauth = OauthManager()
        rest = RestClient()
        router = RouterService()
        // Setup the app with server
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    @IBAction func logoutButtonPushed(sender: UIButton) {
        self.oauth!.heimdallr.clearAccessToken()
        self.performSegueWithIdentifier(SegueStepEnum.LOGOUT.rawValue, sender: self)
    }
    
    @IBAction func getStartedButtonPushed(sender: UIButton) {
        let token = oauth!.tokenStore.retrieveAccessToken()
        rest?.postStartApplication((token?.accessToken)!, onComplete: { data in
            let step = self.router!.mapApiStepResponseToRoute(data)
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier(step, sender: self)
            }
        })
    }
    
}
