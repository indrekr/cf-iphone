//
//  BalanceViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 12/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class BalanceViewController: UIViewController {
    
    @IBOutlet weak var aprLabel: UILabel!
    @IBOutlet weak var moneyAvailableLabel: UILabel!
    @IBOutlet weak var loanBalanceLabel: UILabel!
    @IBOutlet weak var borrowButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var paybackButton: UIButton!
    
    var rest : RestClient?
    
    var minBorrowStep : CGFloat = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rest = RestClient()
        let styler = StylingService()
        
        styler.styleButton(paybackButton, cornerRadius: 25.0)
        styler.styleButton(infoButton, cornerRadius: 25.0)
        styler.styleButton(borrowButton, cornerRadius: 25.0)
        
        getUserData()
    }
    
    func getUserData() {
        rest?.getUserData({ data in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.aprLabel.text  = data["aprText"] as? String
                self.loanBalanceLabel.text = data["loanBalanceText"] as? String
                if (data["balance"] as! CGFloat == 0) {
                    self.paybackButton.enabled = false
                }
            })
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func borrowButtonPushed(sender: UIButton) {
        rest!.requestLoan(minBorrowStep, onComplete : { data in
            self.getUserData()
        })
    }
    @IBAction func infoButtonPushed(sender: UIButton) {
        print("TODO: Show info?")
    }
    @IBAction func paybackButtonPushed(sender: UIButton) {
        print("TODO: Payoff")
    }
    
}
