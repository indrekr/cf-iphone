//
//  Application2ViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 12/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class Application2ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var countryPicker: UIPickerView!
    @IBOutlet weak var addressInput: UITextField!
    @IBOutlet weak var cityInput: UITextField!
    @IBOutlet weak var postalCodeInput: UITextField!
    
    @IBOutlet weak var continueButton: UIButton!
    
    var countryMapData: [String: String] = [String:String]()
    var countryData: [String] = [String]()
    
    var rest : RestClient?
    var router : RouterService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rest = RestClient()
        router = RouterService()
        
        let styler = StylingService()
        styler.styleButton(continueButton)
        
        // Input data into the Array:
        countryMapData = ["United Kingdom": "GB", "Estonia": "EE"]
        countryData = countryMapData.keys.reverse()
        
        self.countryPicker.userInteractionEnabled = false
        
        rest?.getStepData({ data in
            let country = data["country"] as! String
            let rowIndex = self.countryData.indexOf(country)
            dispatch_async(dispatch_get_main_queue()) {
                self.countryPicker.selectRow(rowIndex!, inComponent: 0, animated: true)
            }
        })
        
        // Connect data:
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryData[row]
    }
    
    @IBAction func continueButtonPushed(sender: UIButton) {
        var request : [String:String] = [String:String]()
        
        request["ADDRESS"] = addressInput.text
        request["CITY"] = cityInput.text
        request["POSTAL_CODE"] = postalCodeInput.text
        
        rest?.putStepData(request, onComplete: { data in
            let step = self.router?.mapApiStepResponseToRoute(data)
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier(step!, sender: self)
            }
        })
        
    }
    
    
}