//
//  SegueStepEnum.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 11/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

enum SegueStepEnum : String {
    case
        GET_STARTED = "goToGetStarted",
        LOGIN = "goToLogin",
        REGISTER = "goToRegister",
        LOGOUT = "logout",
        STEP1_PERSONAL_INFO = "goToApplicationStep1",
        STEP2_ADDRESS = "goToApplicationStep2",
        STEP3_ID_UPLOAD = "goToApplicationStep3",
        STEP4_BANK_DETAILS = "goToApplicationStep4",
        WAITING_FOR_OFFER = "goToWaitingForOffer",
        DASHBOARD = "goToDashboard"
}