//
//  RouterService.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 11/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class RouterService {
    
    var oauth : OauthManager?
    var restClient : RestClient?
    
    init() {
        oauth = OauthManager()
        restClient = RestClient()
    }
    
    func mapApiStepResponseToRoute(stepData: AnyObject) -> String {
        let step = stepData["goToStep"] as! String
        
        print("Searching for step map : " + step)
        
        if (step == "GET_STARTED") {
            return SegueStepEnum.GET_STARTED.rawValue
        } else if (step == "STEP1_PERSONAL_INFO") {
            return SegueStepEnum.STEP1_PERSONAL_INFO.rawValue
        } else if (step == "STEP2_ADDRESS") {
            return SegueStepEnum.STEP2_ADDRESS.rawValue
        } else if (step == "STEP3_ID_UPLOAD") {
            return SegueStepEnum.STEP3_ID_UPLOAD.rawValue
        } else if (step == "STEP4_BANK_DETAILS") {
            return SegueStepEnum.STEP4_BANK_DETAILS.rawValue
        } else if (step == "WAITING_PROCESSING") {
            return SegueStepEnum.WAITING_FOR_OFFER.rawValue
        } else if (step == "DASHBOARD") {
            return SegueStepEnum.DASHBOARD.rawValue
        }
        return SegueStepEnum.GET_STARTED.rawValue
    }
    
    func getStepFromApiAndMapToRoute(onComplete: (data: String) -> ()) {
        // If not logged in, will probably throw some shit all over the place, only call when you have a token,
        // check the existence of a token with the method below "isLoggedIn"
        let token = oauth!.tokenStore.retrieveAccessToken()
        self.restClient!.getStep((token?.accessToken)!, onComplete: { data in
            let seg = self.mapApiStepResponseToRoute(data)
            onComplete(data: seg)
        })
    }
    
    func isLoggedIn() -> Bool {
        let hasToken = oauth!.heimdallr.hasAccessToken
        return hasToken
    }
    
}


