//
//  ApiEndpointEnum.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 20/01/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

enum ApiEndpointEnum : String {
    case
        API_DOMAIN = "http://localhost:8080",
        OAUTH_ENDPOINT = "/api/v1/oauth/token",
        ACCOUNT_ENDPOINT = "/api/v1/customer/",
        ACCOUNT_INFO = "info",
        GET_STEP = "getStep",
        START_APPLICATION = "startApplication",
        SUBMIT_DATA = "details",
        UPLOAD_FILE = "upload",
        ID_DOCS_DONE = "idDocs",
        FORM_CRITERIA = "formCriteria",
        REQUEST_LOAN = "requestLoan",
        STEP_DATA = "stepData",
        POST_BANK_DETAILS = "postBankDetails"
}
