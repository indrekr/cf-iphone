//
//  LoginController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 22/01/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class LoginController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var serverResponseLabel: UILabel!
    
    var oauth: OauthManager?
    var rest: RestClient?
    var router: RouterService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let styler = StylingService()
        // Customize buttons
        styler.styleButton(loginButton)
        
        // Setup the app with server
        oauth = OauthManager()
        rest = RestClient()
        router = RouterService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    @IBAction func loginButtonPushed(sender: UIButton) {
        self.oauth?.getUserAccessToken(emailTextField.text!, password: passwordTextField.text!, onComplete: { token in
            self.rest?.getStep(token.accessToken, onComplete: { data in
                let goTo = self.router?.mapApiStepResponseToRoute(data)
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier(goTo!, sender: self)
                }
            })
        })
    }
    
}