//
//  Application3ViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 12/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class Application3ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var selectImageButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var imageView: UIImageView!
    
    var rest : RestClient?
    var router : RouterService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rest = RestClient()
        router = RouterService()
        let styler = StylingService()
        styler.styleButton(selectImageButton)
        styler.styleButton(uploadButton)
        
        uploadButton.enabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func selectImageButtonPushed(sender: UIButton) {
        
        let ImagePicker = UIImagePickerController()
        ImagePicker.delegate = self
        ImagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        self.presentViewController(ImagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismissViewControllerAnimated(true, completion: nil)
        uploadButton.enabled = true
    }
    @IBAction func uploadButtonPushed(sender: UIButton) {
        let image_data = UIImagePNGRepresentation(imageView.image!)
        self.disableAllButtons()
        rest!.uploadFile(image_data!, mimeType: "image/png", onComplete : { data in
            self.continueToNextStep()
        })
    }
    
    func disableAllButtons() {
        selectImageButton.enabled = false
        uploadButton.enabled = false
    }
    
    func continueToNextStep() {
        rest!.postIdDocsDone({ data in
            let step = self.router?.mapApiStepResponseToRoute(data)
            dispatch_async(dispatch_get_main_queue()) {
                self.performSegueWithIdentifier(step!, sender: self)
            }
        })
    }
    
}
