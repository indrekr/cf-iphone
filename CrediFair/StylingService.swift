//
//  StylingService.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 12/05/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit

class StylingService {
    
    func styleButton(button: UIButton, cornerRadius: CGFloat = 5.0) {
        button.layer.cornerRadius = cornerRadius;
        button.layer.borderColor = UIColor.blueColor().CGColor
        button.layer.borderWidth = 1
    }
    
}