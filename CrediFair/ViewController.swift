//
//  ViewController.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 18/01/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit
import Heimdallr

class ViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var titleNameLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let oauth = OauthManager()
        oauth.heimdallr.clearAccessToken()
        
        // Check if we can skip login screen
        let router = RouterService()
        if (router.isLoggedIn()) {
            router.getStepFromApiAndMapToRoute({ data in
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier(data, sender: self)
                }
            })
        }
        
        let styler = StylingService()
        // Customize buttons
        styler.styleButton(loginButton)
        styler.styleButton(registerButton)
        
        // Setup the app with server()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    @IBAction func loginButtonPushed(sender: UIButton) {
        // Go to login screen
        self.performSegueWithIdentifier(SegueStepEnum.LOGIN.rawValue, sender: self)
    }

    @IBAction func registerButtonPushed(sender: UIButton) {
        self.performSegueWithIdentifier(SegueStepEnum.REGISTER.rawValue, sender: self)
    }
}

