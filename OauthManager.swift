//
//  OauthManager.swift
//  CrediFair
//
//  Created by Indrek Ruubel on 20/01/2016.
//  Copyright © 2016 Indrek Ruubel. All rights reserved.
//

import UIKit
import Heimdallr

class OauthManager {
    
    let API_USER = "api-client"
    let API_PASS = "12345"
    var heimdallr: Heimdallr
    var tokenStore: OAuthAccessTokenStore
    var credentials: OAuthClientCredentials
    
    init() {
        let tokenURL = NSURL(string: ApiEndpointEnum.API_DOMAIN.rawValue + ApiEndpointEnum.OAUTH_ENDPOINT.rawValue)!
        credentials = OAuthClientCredentials(id: API_USER, secret: API_PASS)
        tokenStore = OAuthAccessTokenKeychainStore()
        heimdallr = Heimdallr(tokenURL: tokenURL, credentials: credentials, accessTokenStore: tokenStore)
    }
    
    func getUserAccessToken(email: String, password: String, onComplete: (data: OAuthAccessToken) -> ()) {
        let hasToken = heimdallr.hasAccessToken
        if (hasToken) {
            print("Get token from store")
            let token = tokenStore.retrieveAccessToken()
            onComplete(data: token!)
        } else {
            print("Fetch token from API")
            heimdallr.requestAccessToken(username: email, password: password) { result in
                switch result {
                case .Success:
                    let accessToken = self.tokenStore.retrieveAccessToken()
                    onComplete(data: accessToken!)
                case .Failure(_):
                    print("failed getting user access token")
                }
            }
        }
    }
    
    func getClientAuthorization() -> String {
        let apiLoginString = NSString(format: "%@:%@", API_USER, API_PASS)
        let apiLoginData = apiLoginString.dataUsingEncoding(NSUTF8StringEncoding)!
        return apiLoginData.base64EncodedStringWithOptions([])
    }
    
}